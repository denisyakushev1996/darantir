﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BatchApp
{
    public partial class MainPage : ContentPage
    {
        String batchText = "";
         
        public MainPage()
        {
            InitializeComponent();
        }
        void EditorCompleted(object sender, EventArgs e)
        {
            var text = ((Editor)sender).Text;
            batchText = text;
        }
        private async void Button_Clicked(object sender, EventArgs e)
        {
            //System.Diagnostics.Debug.WriteLine(batchText);
            await Navigation.PushAsync(new ChooseAlgorithms());
        }
    }
}
