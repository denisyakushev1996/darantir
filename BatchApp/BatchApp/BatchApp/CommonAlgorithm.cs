﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BatchApp
{
    interface IAlgorithm
    {
        String Name { get; set; }
        DateTime Execute();
    }

    class CommonAlgorithm
    {
    }
}
