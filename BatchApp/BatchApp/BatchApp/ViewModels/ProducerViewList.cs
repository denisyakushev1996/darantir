﻿using BatchApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BatchApp.ViewModels
{
    public class ProducerViewList : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private static List<String> gettingProducer = new List<string>();
        public List<String> WorkWithProducer
        {
            get { return gettingProducer; }
            set { gettingProducer = value; OnPropertyChanged("ArrayListOfProducer"); }
        }
        public List<string> ArrayListOfProducer
        {
            get { return ProducerViewListModel.GetList(); }
        } 
    }
}
